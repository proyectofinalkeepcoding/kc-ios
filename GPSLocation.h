//
//  GPSLocation.h
//  FrikiPop
//
//  Created by Yago de la Fuente on 4/2/16.
//  Copyright © 2016 Saiyens. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
@class CoreLocation;
@interface GPSLocation : NSObject <CLLocationManagerDelegate>

-(void)currentLocationWithBlock:(void(^)(CLLocation *currentLocation))completion;

@end
