//
//  Image.h
//  FrikiPop
//
//  Created by Yago de la Fuente on 23/1/16.
//  Copyright © 2016 Saiyens. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>

@interface Image : MTLModel<MTLJSONSerializing>

@property (copy, nonatomic) NSString *urlImage;

@end
