//
//  ServiceUtils.m
//  FrikiPop
//
//  Created by Yago de la Fuente on 16/1/16.
//  Copyright © 2016 Saiyens. All rights reserved.
//

#import "ServiceUtils.h"

@implementation ServiceUtils


+(NSString *) addParamToPath: (NSString *)path parameter: (NSString *) parameter {
    
    return [NSString stringWithFormat:@"%@%@/", path, parameter];
}

@end
