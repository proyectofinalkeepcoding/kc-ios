//
//  SingInViewController.m
//  FrikiPop
//
//  Created by Rafael Navarro on 12/1/16.
//  Copyright © 2016 Saiyens. All rights reserved.
//

#import "SingInViewController.h"
#import "RegisterUserViewModelController.h"
#import "NewUser.h"
#import "UserProfile.h"

@interface SingInViewController ()
@property (weak, nonatomic) IBOutlet UITextField *usuarioField;
@property (weak, nonatomic) IBOutlet UITextField *apellidosField;
@property (weak, nonatomic) IBOutlet UITextField *emailField;
@property (weak, nonatomic) IBOutlet UITextField *passField;
@property (weak, nonatomic) IBOutlet UITextField *passRepeatedField;
@property (weak, nonatomic) IBOutlet UITextField *direccionField;
@property (weak, nonatomic) IBOutlet UITextField *codigoField;
@property (weak, nonatomic) IBOutlet UITextField *provinciaField;
@property (strong, nonatomic) RegisterUserViewModelController *viewModel;
@property float latitude;
@property float longitude;
@property (weak, nonatomic) IBOutlet UITextField *nombreField;

@end

@implementation SingInViewController

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    
    
    if (self = [super initWithNibName:nibNameOrNil
                               bundle:nibBundleOrNil]) {
        _viewModel = [[RegisterUserViewModelController alloc]init];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = NO;
    self.title = @"Registro";
    
    self.usuarioField.delegate = self;
    self.apellidosField.delegate = self;
    self.passField.delegate = self;
    self.emailField.delegate = self;
    self.passRepeatedField.delegate = self;
    self.direccionField.delegate = self;
    self.codigoField.delegate = self;
    self.provinciaField.delegate = self;
    self.nombreField.delegate = self;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Registrar"
                                                                             style:UIBarButtonItemStyleDone
                                                                            target:self
                                                                            action:@selector(registerUser)];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.

    
}

- (IBAction)hideKeyboard:(id)sender {
    
    [self.view endEditing:YES];
    
}

#pragma mark - textfielddelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    //Valido el texto
    NSLog(@"Texto del label: %@", textField.text);
    
    [textField resignFirstResponder];
    return YES;
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    //El texto ha sido validado
    NSLog(@"El texto es: %@", textField.text);
    
}

-(void)registerUser {
    
    NewUser *user = [NewUser new];
    user.username = self.usuarioField.text;
    user.firstName = self.nombreField.text;
    user.lastName = self.apellidosField.text;
    user.email = self.emailField.text;
    user.password = self.passField.text;
    UserProfile *profile = [UserProfile new];
    profile.longitude = self.longitude;
    profile.latitude = self.latitude;
    profile.address = self.direccionField.text;
    profile.zipcode = self.codigoField.text;
    profile.city = self.provinciaField.text;
    user.userProfile = profile;
    [self.viewModel registerUser:user];

}

- (IBAction)localizame:(id)sender {
    
    [self.viewModel currentLocationWithBlock:^(CLLocation *currentLocation) {
        CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
        [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error)
         {
             if (!(error))
             {
                 CLPlacemark *placemark = [placemarks objectAtIndex:0];
                 NSLog(@"\nCurrent Location Detected\n");
                 NSLog(@"placemark %@",placemark);
                 NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
                 
                 NSString *address = [[NSString alloc]initWithString:locatedAt];
                 NSString *zipCode = [[NSString alloc]initWithString:placemark.postalCode];
               
                 self.codigoField.text = zipCode;
                 self.direccionField.text = address;
                 self.provinciaField.text = [NSString stringWithFormat:@"%@", placemark.locality];
                 self.latitude = currentLocation.coordinate.latitude;
                 self.longitude = currentLocation.coordinate.longitude;
             }
             else
             {
                 NSLog(@"Geocode failed with error %@", error); // Error handling must required
             }
         }];
        
    }];
}

@end
