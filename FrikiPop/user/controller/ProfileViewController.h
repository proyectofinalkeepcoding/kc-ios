//
//  ProfileViewController.h
//  FrikiPop
//
//  Created by Yago de la Fuente on 14/1/16.
//  Copyright © 2016 Saiyens. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource>

@end
