//
//  ProfileViewController.m
//  FrikiPop
//
//  Created by Yago de la Fuente on 14/1/16.
//  Copyright © 2016 Saiyens. All rights reserved.
//

#import "ProfileViewController.h"
#import "ProductsCollectionViewDataSource.h"
#import "ProductsCollectionViewCell.h"
#import "SessionManager.h"
#import "UserService.h"
#import "User.h"
#import "UserProfile.h"
#import "LocationUtils.h"
#import <UIImageView+AFNetworking.h>
#import "ProductService.h"
#import "DetailProductViewController.h"


@interface ProfileViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberTransactionsLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) ProductsCollectionViewDataSource *customDataSource;
@property (strong, nonatomic) User *user;
@property (strong, nonatomic) ProductService *productService;

@end

@implementation ProfileViewController

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    
    if (self = [super initWithNibName:nibNameOrNil
                               bundle:nibBundleOrNil]) {
        _productService = [ProductService new];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self configureView];
    
    @weakify(self)
    [[self.customDataSource fetchUserProducts] subscribeNext:^(id x) {
        @strongify(self);
        [self.collectionView reloadData];
    }];
    
    [[[UserService new]fetchCurrentUser] subscribeNext:^(User *user) {
        self.user = user;
        CLLocation *location = [[CLLocation alloc]initWithLatitude:self.user.userProfile.latitude
                                                         longitude:self.user.userProfile.longitude];
        [LocationUtils getAddressFromLocation:location
                              completionBlock:^(NSString *address) {
                                  self.locationLabel.text = address;
                              }];
        [self.profileImage setImageWithURL:[NSURL URLWithString:self.user.userProfile.avatarURL]
                         placeholderImage:[UIImage imageNamed:@"profile_placeholder.jpg"]];
    }];


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)transactionTypeChanged:(id)sender {
    
}


#pragma mark Private methods

-(void)configureView {
    
    self.collectionView.delegate = self;
    //ProductsCollectionViewDataSource *dataSource = [ProductsCollectionViewDataSource new];
    self.customDataSource = [ProductsCollectionViewDataSource new];

    self.collectionView.dataSource = self;
    //[self.collectionView registerClass:[ProductsCollectionViewCell class] forCellWithReuseIdentifier:@"ProductCell"];
    UINib *cellNib = [UINib nibWithNibName:@"ProductsCollectionViewCell" bundle:nil];

    [self.collectionView registerNib:cellNib
          forCellWithReuseIdentifier:@"ProductCell"];
    
    self.profileImage.layer.cornerRadius = self.profileImage.frame.size.height /2;
    self.profileImage.layer.masksToBounds = YES;
    self.profileImage.layer.borderWidth = 0;
    
    self.usernameLabel.text = [SessionManager sharedInstance].username;
    
    self.navigationItem.title = @"Perfil";

}

#pragma mark <UICollectionViewDataSource>

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return [self.customDataSource numberOfSectionsInCollectionView:collectionView];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.customDataSource collectionView:collectionView
                   numberOfItemsInSection:section];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    return [self.customDataSource collectionView:collectionView
                          cellForItemAtIndexPath:indexPath];
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0.0, 18.0, 0.0, 18.0);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    Product *product = [self.customDataSource productForIndexPath:indexPath];
    
    [[self.productService fecthProductDetail:product.idProduct]subscribeNext:^(ProductDetail *productDetail) {
        
        DetailProductViewController *vc = [[DetailProductViewController alloc]initWithProductDetail:productDetail];
        
        [self.navigationController pushViewController:vc
                                             animated:YES];
        
    }];
}

@end
