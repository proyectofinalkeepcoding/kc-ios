//
//  SingInViewController.h
//  FrikiPop
//
//  Created by Rafael Navarro on 12/1/16.
//  Copyright © 2016 Saiyens. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SingInViewController : UIViewController <UITextFieldDelegate>

@end
