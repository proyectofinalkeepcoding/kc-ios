//
//  ViewController.h
//  FrikiPop
//
//  Created by Rafael Navarro on 24/11/15.
//  Copyright © 2015 Saiyens. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UITextField *userField;


- (IBAction)rememberPass:(id)sender;

- (IBAction)registerUser:(id)sender;

@end
