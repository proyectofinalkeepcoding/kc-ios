//
//  ViewController.m
//  FrikiPop
//
//  Created by Rafael Navarro on 24/11/15.
//  Copyright © 2015 Saiyens. All rights reserved.
//

#import "LoginViewController.h"
#import "SingInViewController.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "LoginResult.h"
#import "LoginViewModelController.h"
#import "SessionManager.h"
#import "Azure.h"
#import "UserService.h"
#import "ProfileViewController.h"
#import "SearchProductViewController.h"
#import "FilterParams.h"
#import "NewProductViewController.h"

@interface LoginViewController ()

@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (strong, nonatomic) LoginViewModelController *viewModel;
@property (strong, nonatomic) Azure *azure;
@property (strong, nonatomic) UserService *service;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    
    // Do any additional setup after loading the view, typically from a nib.
    
    _service = [UserService new];
    
    // FIN BORRAR
    RACSignal *loginEnable = [RACSignal combineLatest:@[self.userField.rac_textSignal, self.passwordField.rac_textSignal]
                                               reduce:^id(NSString *username, NSString *password){
                                                   return @(username.length > 0 && password.length > 0);
                                               }];
    
    self.viewModel = [LoginViewModelController new];
    self.loginButton.rac_command = [self.viewModel loginCommandWithEnableSignal:loginEnable];
    
    RAC(self, viewModel.username) = self.userField.rac_textSignal;
    RAC(self, viewModel.password) = self.passwordField.rac_textSignal;
    
    [[[self.loginButton.rac_command executionSignals]switchToLatest]subscribeNext:^(LoginResult *loginResult) {
        SessionManager *session = [SessionManager sharedInstance];
        [session saveToken:loginResult.token];
        [session saveUsername:loginResult.username];
        
        [[self presentingViewController]dismissViewControllerAnimated:YES
                                                           completion:nil];
    }];
    
    
    [[self.loginButton.rac_command errors]subscribeNext:^(id x) {
        UIAlertController * alert=[UIAlertController
                                   alertControllerWithTitle:@"Error autenticación"
                                   message:@"Usuario o contraseña invalidos"
                                   preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = YES;
    self.userField.delegate = self;
    self.passwordField.delegate = self;
    
}

- (IBAction)hideKeyboard:(id)sender {
    
    [self.view endEditing:YES];
    
}

#pragma mark - textfielddelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    //Valido el texto
    NSLog(@"Texto del label: %@", textField.text);
    
    [textField resignFirstResponder];
    return YES;
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    //El texto ha sido validado
    NSLog(@"El texto es: %@", textField.text);
    
}

- (IBAction)rememberPass:(id)sender {
}

- (IBAction)registerUser:(id)sender {
    
    [self.navigationController pushViewController:[SingInViewController new] animated:YES];
    
}
@end
