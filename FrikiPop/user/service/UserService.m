//
//  UserService.m
//  FrikiPop
//
//  Created by Yago de la Fuente on 25/11/15.
//  Copyright © 2015 Saiyens. All rights reserved.
//

#import "UserService.h"
#import "ApiClient.h"
#import "LoginResult.h"
#import <Mantle/Mantle.h>
#import "ServiceUtils.h"
#import "UserProfile.h"

static NSString *AUTH_TOKEN = @"auth_token";
static NSString *LOGIN_PATH = @"auth/login/";
static NSString *USER_ME_PATH = @"auth/me/";
static NSString *RESET_PASSWORD_PATH = @"password/reset/";
static NSString *CHANGE_PASSWORD_PATH = @"password/";
static NSString *USERS_PATH = @"api/1.0/users/";
static NSString *USER_ID = @"id";

@interface UserService()
@property (strong, nonatomic) ApiClient *apiClient;
@end

@implementation UserService

-(id) init {
    
    if (self = [super init]) {
        _apiClient = [ApiClient sharedInstance];
    }
    
    return self;
    
}

-(RACSignal *)fetchLoginResult:(NSString *)username withPassword:(NSString *)password {
    
    
    RACSignal *loginSignal = [self.apiClient LOGIN:LOGIN_PATH
                                       withParams:@{@"username" : username,
                                                    @"password" : password}];

    return [loginSignal map:^id(NSDictionary *json) {
        NSString *token = json[AUTH_TOKEN];
        return [[LoginResult new] initWithUsername:username token:token];
    }];
    
}

-(RACSignal *)resetPasswordWithMail:(NSString *)email {
    
    RACSignal *resetPasswordSignal = [self.apiClient POST:RESET_PASSWORD_PATH
                                               withParams:@{@"email" : email}
                                       withAuthentication:YES];
    
    return resetPasswordSignal;
    
}

-(RACSignal *) changePassword: (NSString *) currentPassword withNewPassword: (NSString *) newPassword {
    
    NSDictionary *params = @{@"new_password" : newPassword,
                             @"current_password" : currentPassword};
    
    RACSignal *changePasswordSignal = [self.apiClient POST:CHANGE_PASSWORD_PATH
                                                withParams:params
                                        withAuthentication:YES];
    return changePasswordSignal;
}

-(RACSignal *)registerUser:(NewUser *)user {
    
    NSError *error;
    // Error Mantle revisar
    NSDictionary *params = [MTLJSONAdapter JSONDictionaryFromModel:user
                                                           error:&error];
    RACSignal *registerUserSignal = [self.apiClient POST:USERS_PATH
                                              withParams:params
                                      withAuthentication:NO];
    
    return registerUserSignal;
    
}

-(RACSignal *)fetchCurrentUser {
    
    RACSignal *userIDSignal = [self fetchUserID];
    
    return [userIDSignal flattenMap:^RACStream *(NSDictionary *json) {
        NSString *userID = [json[USER_ID] stringValue];
        NSString *path = [ServiceUtils addParamToPath:USERS_PATH
                                    parameter:userID];
        
        RACSignal *userSignal = [self.apiClient GET:path
                        withParams:nil
                withAuthentication:YES];
        
        return [[userSignal map:^id(NSDictionary *userJSON) {
            return [MTLJSONAdapter modelOfClass:[User class]
                             fromJSONDictionary:userJSON
                                          error:nil];
        }]deliverOnMainThread];
    }];
    
}

-(RACSignal *)fetchUserID {
    
    RACSignal *userIDSignal = [self.apiClient GET:USER_ME_PATH
                                       withParams:nil
                               withAuthentication:YES];
    return userIDSignal;
}


@end
