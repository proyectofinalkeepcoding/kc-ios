//
//  UserService.h
//  FrikiPop
//
//  Created by Yago de la Fuente on 25/11/15.
//  Copyright © 2015 Saiyens. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "User.h"
#import "NewUser.h"

@interface UserService : NSObject

-(RACSignal *) fetchLoginResult: (NSString *) username withPassword: (NSString *) password;

-(RACSignal *) resetPasswordWithMail: (NSString *) email;

-(RACSignal *) changePassword: (NSString *) currentPassword withNewPassword: (NSString *) newPassword;

-(RACSignal *) registerUser: (NewUser *)user;

-(RACSignal *)fetchCurrentUser;

-(RACSignal *)fetchUserID;

@end
