//
//  LoginViewModelController.m
//  FrikiPop
//
//  Created by Yago de la Fuente on 25/11/15.
//  Copyright © 2015 Saiyens. All rights reserved.
//

#import "LoginViewModelController.h"
#import "UserService.h"
#import "LoginResult.h"

@implementation LoginViewModelController


-(RACCommand *) loginCommandWithEnableSignal: (RACSignal *) enableSignal {
    
    UserService *service = [UserService new];
    return [[RACCommand alloc] initWithEnabled:enableSignal
                            signalBlock:^RACSignal *(id input) {
                                return [[service fetchLoginResult:self.username withPassword:self.password]deliverOnMainThread];
                            }];
    
}

@end
