//
//  ChangePasswordViewModelController.h
//  FrikiPop
//
//  Created by Yago de la Fuente on 1/12/15.
//  Copyright © 2015 Saiyens. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface ChangePasswordViewModelController : NSObject

@property (copy, nonatomic) NSString *currentPassword;
@property (copy, nonatomic) NSString *givenPassword;

@end
