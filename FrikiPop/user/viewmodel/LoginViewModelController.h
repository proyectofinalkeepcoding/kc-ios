//
//  LoginViewModelController.h
//  FrikiPop
//
//  Created by Yago de la Fuente on 25/11/15.
//  Copyright © 2015 Saiyens. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "LoginResult.h"

@interface LoginViewModelController : NSObject

@property (copy, nonatomic) NSString *username;
@property (copy, nonatomic) NSString *password;
@property (strong, nonatomic) RACCommand *loginCommand;
@property (strong, nonatomic) LoginResult *loginResult;

-(RACCommand *) loginCommandWithEnableSignal: (RACSignal *) enableSignal;

@end
