//
//  ChangePasswordViewModelController.m
//  FrikiPop
//
//  Created by Yago de la Fuente on 1/12/15.
//  Copyright © 2015 Saiyens. All rights reserved.
//

#import "ChangePasswordViewModelController.h"
#import "UserService.h"

@implementation ChangePasswordViewModelController

-(RACCommand *) changePassword:(RACSignal *) enabledSignal{
    
    UserService *service = [UserService new];
    return [[RACCommand alloc] initWithEnabled:enabledSignal
                                   signalBlock:^RACSignal *(id input) {
                                       return [service changePassword:self.currentPassword
                                                      withNewPassword:self.givenPassword];
                                   }];
}

@end
