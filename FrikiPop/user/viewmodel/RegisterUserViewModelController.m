//
//  RegisterUserViewModelController.m
//  FrikiPop
//
//  Created by Yago de la Fuente on 1/12/15.
//  Copyright © 2015 Saiyens. All rights reserved.
//

#import "RegisterUserViewModelController.h"
#import "UserService.h"
#import <CoreLocation/CoreLocation.h>
#import "GPSLocation.h"

@interface RegisterUserViewModelController ()
@property (strong, nonatomic) GPSLocation *gps;
@end

@implementation RegisterUserViewModelController

-(id)init {
    
    if (self = [super init]) {
         _gps = [[GPSLocation alloc]init];
    }
    return self;
}

-(RACCommand *) registerUserCommand: (RACSignal *) enabledSignal {
    
    UserService *service = [UserService new];
    
    return [[RACCommand alloc] initWithEnabled:enabledSignal
                            signalBlock:^RACSignal *(id input) {
                                self.user.userProfile = self.userProfile;
                                return [service registerUser:self.user];
                            }];
    
}


-(void)currentLocationWithBlock:(void(^)(CLLocation *currentLocation))completion{
        [self.gps currentLocationWithBlock:completion];
}

-(void)registerUser:(NewUser *)user {
    RACSignal *registerSignal = [[UserService new]registerUser:user];
    [registerSignal subscribeNext:^(id x) {
        NSLog(@"Usuario registrado %@", x);
    }];
    [registerSignal subscribeError:^(NSError *error) {
        NSLog(@"Error al registrar al usuario %@", error);
    }];
}

@end
