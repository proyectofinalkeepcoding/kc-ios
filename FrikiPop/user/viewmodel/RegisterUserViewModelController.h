//
//  RegisterUserViewModelController.h
//  FrikiPop
//
//  Created by Yago de la Fuente on 1/12/15.
//  Copyright © 2015 Saiyens. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserProfile.h"
#import "NewUser.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface RegisterUserViewModelController : NSObject

@property (strong, nonatomic) User *user;
@property (strong, nonatomic) UserProfile *userProfile;

-(RACCommand *) registerUserCommand: (RACSignal *) enabledSignal;
-(void)registerUser:(NewUser *)user;
-(void)currentLocationWithBlock:(void(^)(CLLocation *currentLocation))completion;

@end
