//
//  User.m
//  FrikiPop
//
//  Created by Yago de la Fuente on 1/12/15.
//  Copyright © 2015 Saiyens. All rights reserved.
//

#import "User.h"
#import "UserProfile.h"
@implementation User

+(NSDictionary *)JSONKeyPathsByPropertyKey {
    
    return @{@"username" : @"username",
             @"email" : @"email",
             @"firstName" : @"first_name",
             @"lastName" : @"last_name",
             @"userProfile" : @"user_profile"};
    
}

+ (NSValueTransformer *)userProfileJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[UserProfile class]];
}


@end
