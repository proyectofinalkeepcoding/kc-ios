//
//  UserProfile.h
//  FrikiPop
//
//  Created by Yago de la Fuente on 1/12/15.
//  Copyright © 2015 Saiyens. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface UserProfile : MTLModel<MTLJSONSerializing>

@property (copy, nonatomic) NSString *avatarURL;
@property float latitude;
@property float longitude;
@property (copy, nonatomic) NSString *address;
@property (copy, nonatomic) NSString *zipcode;
@property (copy, nonatomic) NSString *city;
@property (copy, nonatomic) NSString *state;
@property (copy, nonatomic) NSString *country;
@property (copy, nonatomic) NSString *phone;

@end
