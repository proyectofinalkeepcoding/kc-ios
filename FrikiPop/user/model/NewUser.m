//
//  NewUser.m
//  FrikiPop
//
//  Created by Yago de la Fuente on 4/2/16.
//  Copyright © 2016 Saiyens. All rights reserved.
//

#import "NewUser.h"
#import "UserProfile.h"

@implementation NewUser

+(NSDictionary *)JSONKeyPathsByPropertyKey {
    
    return @{@"username" : @"username",
             @"password" : @"password",
             @"email" : @"email",
             @"firstName" : @"first_name",
             @"lastName" : @"last_name",
             @"userProfile" : @"user_profile"};
    
}

+ (NSValueTransformer *)userProfileJSONTransformer {
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[UserProfile class]];
}

@end
