//
//  LoginResult.h
//  FrikiPop
//
//  Created by Yago de la Fuente on 25/11/15.
//  Copyright © 2015 Saiyens. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LoginResult : NSObject

@property (copy, nonatomic) NSString *username;
@property (copy, nonatomic) NSString *token;

-(id) initWithUsername: (NSString *) username token: (NSString *) token;

@end
