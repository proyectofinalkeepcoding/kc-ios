//
//  NewUser.h
//  FrikiPop
//
//  Created by Yago de la Fuente on 4/2/16.
//  Copyright © 2016 Saiyens. All rights reserved.
//

#import "User.h"
#import <Mantle/Mantle.h>

@interface NewUser : MTLModel<MTLJSONSerializing>

@property (strong, nonatomic) NSString *password;
@property (copy, nonatomic) NSString *username;
@property (copy, nonatomic) NSString *email;
@property (copy, nonatomic) NSString *firstName;
@property (copy, nonatomic) NSString *lastName;
@property (strong, nonatomic) UserProfile *userProfile;

@end
