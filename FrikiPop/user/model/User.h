//
//  User.h
//  FrikiPop
//
//  Created by Yago de la Fuente on 1/12/15.
//  Copyright © 2015 Saiyens. All rights reserved.
//

#import <Mantle/Mantle.h>
@class UserProfile;

@interface User : MTLModel<MTLJSONSerializing>

@property (copy, nonatomic) NSString *username;
@property (copy, nonatomic) NSString *email;
@property (copy, nonatomic) NSString *firstName;
@property (copy, nonatomic) NSString *lastName;
@property (strong, nonatomic) UserProfile *userProfile;

@end
