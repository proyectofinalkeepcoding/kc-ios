//
//  LoginResult.m
//  FrikiPop
//
//  Created by Yago de la Fuente on 25/11/15.
//  Copyright © 2015 Saiyens. All rights reserved.
//

#import "LoginResult.h"

@implementation LoginResult

-(id)initWithUsername:(NSString *)username token:(NSString *)token {

    if ( self = [super init]) {
        self.username = username;
        self.token = token;
    }
    
        return self;
}

@end
