//
//  UserProfile.m
//  FrikiPop
//
//  Created by Yago de la Fuente on 1/12/15.
//  Copyright © 2015 Saiyens. All rights reserved.
//

#import "UserProfile.h"

@implementation UserProfile

+(NSDictionary *)JSONKeyPathsByPropertyKey {
    
    return @{@"avatarURL" : @"avatar_url",
             @"latitude" : @"latitude",
             @"longitude" : @"longitude",
             @"address" : @"address",
             @"zipcode" : @"zip_code",
             @"city" : @"city",
             @"state" : @"state",
             @"country" : @"country",
             @"phone" : @"phone_number"};

}

@end
