//
//  SessionManager.h
//  FrikiPop
//
//  Created by Yago de la Fuente on 26/11/15.
//  Copyright © 2015 Saiyens. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SessionManager : NSObject

@property (copy, nonatomic) NSString *username;

+(instancetype) sharedInstance;

-(void) saveToken:(NSString *)token;
-(void)saveUsername:(NSString *)username;
-(NSString *) token;



@end
