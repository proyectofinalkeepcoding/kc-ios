//
//  LocationUtils.h
//  FrikiPop
//
//  Created by Yago de la Fuente on 21/1/16.
//  Copyright © 2016 Saiyens. All rights reserved.
//

#import <Foundation/Foundation.h>
@import CoreLocation;

@interface LocationUtils : NSObject
+(void)getAddressFromLocation:(CLLocation *)location completionBlock:(void(^)(NSString *address))completionBlock;
@end
