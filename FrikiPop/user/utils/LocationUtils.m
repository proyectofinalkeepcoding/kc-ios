//
//  LocationUtils.m
//  FrikiPop
//
//  Created by Yago de la Fuente on 21/1/16.
//  Copyright © 2016 Saiyens. All rights reserved.
//

#import "LocationUtils.h"

@implementation LocationUtils

+(void)getAddressFromLocation:(CLLocation *)location completionBlock:(void(^)(NSString *))completionBlock
{
    __block CLPlacemark* placemark;
    __block NSString *address = nil;
    
    CLGeocoder* geocoder = [CLGeocoder new];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (error == nil && [placemarks count] > 0)
         {
             placemark = [placemarks lastObject];
             address = [NSString stringWithFormat:@"%@, %@ %@", placemark.name, placemark.postalCode, placemark.locality];
             dispatch_async(dispatch_get_main_queue(), ^{
                 completionBlock(address);
             });
         }
     }];
}

@end
