//
//  SessionManager.m
//  FrikiPop
//
//  Created by Yago de la Fuente on 26/11/15.
//  Copyright © 2015 Saiyens. All rights reserved.
//

#import "SessionManager.h"

static NSString *TOKEN = @"TOKEN_KEY";
static NSString *USERNAME = @"USER_KEY";


@interface SessionManager ()

@property (copy, nonatomic) NSString *token;

@end

@implementation SessionManager

+ (id)sharedInstance {
    static SessionManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

-(void)saveToken:(NSString *)token {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:token
                 forKey:TOKEN];
    [defaults synchronize];
    
}

-(void)saveUsername:(NSString *)username {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:username
                 forKey:USERNAME];
    [defaults synchronize];
}

-(NSString *)token {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:TOKEN];
    
}

-(NSString *)username {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:USERNAME];
    
}

@end
