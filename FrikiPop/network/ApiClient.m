//
//  SAIApiClient.m
//  FrikiPop
//
//  Created by Yago de la Fuente on 25/11/15.
//  Copyright © 2015 Saiyens. All rights reserved.
//

#import "ApiClient.h"
#import "SessionManager.h"

static NSString *URL_BASE = @"http://frikipop.cloudapp.net:8080/";

@interface ApiClient()

@property (strong, nonatomic) AFHTTPRequestOperationManager * requestManager;

@end

@implementation ApiClient

+ (id)sharedInstance {
    static ApiClient *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (id)init {
    
    if (self = [super init]) {
        _requestManager = [[AFHTTPRequestOperationManager alloc]
                           initWithBaseURL:[NSURL URLWithString:URL_BASE]];
    }
    return self;
}

-(RACSignal *)POST:(NSString *)path withParams: (NSDictionary *) params withAuthentication: (BOOL) authentication{
    
    if (authentication) {
        [self addTokenToRequest];
    }
    //self.requestManager.requestSerializer = [[AFJSONRequestSerializer alloc]init];
    return [[RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        
        AFHTTPRequestOperation *operation = [self.requestManager POST:path
                                                           parameters:params
                                                              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                                  
                                                                  [subscriber sendNext:responseObject];
                                                                  [subscriber sendCompleted];
                                                                  
                                                              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                                  
                                                                  [subscriber sendError:error];
                                                                  
                                                              }];
        
        return [RACDisposable disposableWithBlock:^{
            [operation cancel];
        }];
        
    }] deliverOn:[RACScheduler scheduler]];
    
}

-(RACSignal *)LOGIN:(NSString *)path withParams: (NSDictionary *) params{
    
    self.requestManager.requestSerializer = [[AFHTTPRequestSerializer alloc]init];
    return [[RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        
        AFHTTPRequestOperation *operation = [self.requestManager POST:path
                                                           parameters:params
                                                              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                                  
                                                                  [subscriber sendNext:responseObject];
                                                                  [subscriber sendCompleted];
                                                                  
                                                              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                                  
                                                                  [subscriber sendError:error];
                                                                  
                                                              }];
        
        return [RACDisposable disposableWithBlock:^{
            [operation cancel];
        }];
        
    }] deliverOn:[RACScheduler scheduler]];
    
}

-(RACSignal *)GET:(NSString *)path withParams: (NSDictionary *) params withAuthentication: (BOOL) authentication{
    
    return [[RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        
        if (authentication) {
            [self addTokenToRequest];
        }
        AFHTTPRequestOperation *operation = [self.requestManager GET:path
                                                          parameters:params
                                                             success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                              
                                                              [subscriber sendNext:responseObject];
                                                              [subscriber sendCompleted];
                                                              
                                                          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                              
                                                              [subscriber sendError:error];
                                                              
                                                          }];
        
        return [RACDisposable disposableWithBlock:^{
            [operation cancel];
        }];
        
    }] deliverOn:[RACScheduler scheduler]];
    
}

-(RACSignal *)PUT:(NSString *)path withParams: (NSDictionary *) params withAuthentication: (BOOL) authentication{
    
    return [[RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        
        if (authentication) {
            [self addTokenToRequest];
        }
        
      
        AFHTTPRequestOperation *operation = [self.requestManager PUT:path
                                                          parameters:params
                                                             success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                                 
                                                                 [subscriber sendNext:responseObject];
                                                                 [subscriber sendCompleted];
                                                                 
                                                             } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                                 
                                                                 [subscriber sendError:error];
                                                                 
                                                             }];
        
        return [RACDisposable disposableWithBlock:^{
            [operation cancel];
        }];
        
    }] deliverOn:[RACScheduler scheduler]];
    
}

#pragma mark Private methods

-(void) addTokenToRequest {
    
    SessionManager *session = [SessionManager sharedInstance];
    
    [self.requestManager.requestSerializer setValue:[NSString stringWithFormat:@"token %@", [session token]] forHTTPHeaderField:@"Authorization"];
}

@end
