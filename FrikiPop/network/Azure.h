//
//  Azure.h
//  FrikiPop
//
//  Created by Yago de la Fuente on 29/12/15.
//  Copyright © 2015 Saiyens. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Azure : NSObject

+(id)sharedInstance;

-(void)saveBlobWithName: (NSString *) blobName data: (NSData *)data completion:(void (^)(NSURL * url))completionBlock;

-(void)blobURLWithBlobName: (NSString *) blobName completion: (void (^)(NSURL * url))completionBlock;


@end
