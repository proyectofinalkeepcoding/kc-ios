//
//  ApiClient.h
//  FrikiPop
//
//  Created by Yago de la Fuente on 25/11/15.
//  Copyright © 2015 Saiyens. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface ApiClient : NSObject

-(RACSignal *)POST:(NSString *)path withParams: (NSDictionary *) params withAuthentication: (BOOL) authentication;
-(RACSignal *)GET:(NSString *)path withParams: (NSDictionary *) params withAuthentication: (BOOL) authentication;
-(RACSignal *)PUT:(NSString *)path withParams: (NSDictionary *) params withAuthentication: (BOOL) authentication;
-(RACSignal *)LOGIN:(NSString *)path withParams: (NSDictionary *) params;
+(id) sharedInstance;

@end
