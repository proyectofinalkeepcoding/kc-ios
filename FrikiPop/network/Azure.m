//
//  Azure.m
//  FrikiPop
//
//  Created by Yago de la Fuente on 29/12/15.
//  Copyright © 2015 Saiyens. All rights reserved.
//

#import "Azure.h"
#import <WindowsAzureMobileServices/WindowsAzureMobileServices.h>
#import <AFNetworking/AFNetworking.h>

static NSString *AZURE_URL = @"https://saiyans-imagenes.azure-mobile.net/";
static NSString *AZURE_KEY = @"welYTDcWVtIVMasXopVpsnwJbHFLbp50";
static NSString *BLOB_NAME = @"blobName";
static NSString *CONTAINER_NAME = @"containerName";

@interface Azure ()

@property (strong, nonatomic) MSClient *client;
@property (strong, nonatomic) NSURLSession *session;

@end

@implementation Azure

+ (id)sharedInstance {
    static Azure *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (id)init {
    
    if (self = [super init]) {
        [self warmupMSClient];
        _session = [NSURLSession sharedSession];
    }
    
    return self;
}


-(void)saveBlobWithName: (NSString *) blobName data: (NSData *) data completion: (void (^)(NSURL * url))completionBlock{
    
    [self blobURLWithBlobName:blobName completion:^(NSURL *url) {
        
        NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:url];
        
        request.HTTPMethod = @"PUT";
        [request setValue:@"image/jpg" forHTTPHeaderField:@"Content-Type"];
        [request setValue:@"BlockBlob" forHTTPHeaderField:@"x-ms-blob-type"];
        [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)data.length] forHTTPHeaderField:@"Content-Length"];
        
        NSURLSessionUploadTask * uploadTask = [_session uploadTaskWithRequest:request
                                                                     fromData:data
                                                            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                                NSLog(@"Respuesta: %@",                                                                 response.URL);
                                                                completionBlock(response.URL);
                                                                if (error) {
                                                                    NSLog(@"Error al subir el blob %@", error);
                                                                }
                                                            }];
        [uploadTask resume];
    }];
    
}

-(void) blobURLWithBlobName: (NSString *) blobName completion: (void (^)(NSURL * url))completionBlock{
    
    [self warmupMSClient];
    
    NSDictionary *dict = @{ BLOB_NAME : blobName,
                            CONTAINER_NAME : @"imagenes"};
    
    [_client invokeAPI:@"generatesasurl"
                  body:nil
            HTTPMethod:@"GET"
            parameters:dict
               headers:nil
            completion:^(id result, NSHTTPURLResponse *response, NSError *error) {
                
                if (error) {
                    NSLog(@"Error al recuperar url del blob: %@", error);
                } else {
                    NSString *urlName = [result objectForKey:@"sasUrl"];
                    completionBlock([NSURL URLWithString:urlName]);
                }
            }];
    
}

#pragma mark - Azure setup

-(void)warmupMSClient{
    
    _client = [MSClient clientWithApplicationURL:[NSURL URLWithString:AZURE_URL]
                                  applicationKey:AZURE_KEY];
    
}




@end
