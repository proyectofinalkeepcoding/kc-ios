//
//  Image.m
//  FrikiPop
//
//  Created by Yago de la Fuente on 23/1/16.
//  Copyright © 2016 Saiyens. All rights reserved.
//

#import "Image.h"

@implementation Image

+(NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"urlImage" : @"url"};
}

@end
