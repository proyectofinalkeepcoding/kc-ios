//
//  CategoryService.m
//  FrikiPop
//
//  Created by Yago de la Fuente on 18/1/16.
//  Copyright © 2016 Saiyens. All rights reserved.
//

#import "CategoryService.h"
#import "ApiClient.h"
#import <Mantle/Mantle.h>
#import "ProductCategory.h"

@interface CategoryService ()
@property (strong, nonatomic) ApiClient *apiClient;
@end

@implementation CategoryService

static NSString *PRODUCTS_PATH = @"api/1.0/categories/";

-(id)init {
    
    if (self = [super init]) {
        _apiClient = [ApiClient sharedInstance];
    }
    return self;
}


-(RACSignal *)fetchCategories {
    
    
    RACSignal *categoriesSignal = [self.apiClient GET:PRODUCTS_PATH
                                             withParams:nil
                                     withAuthentication:YES];
    
    return [[categoriesSignal map:^id(NSArray *JsonCategories) {
        ProductCategory *defaultCategory = [ProductCategory new];
        defaultCategory.name = @"Todas";
        NSMutableArray *categoriesWithDefaultOption = [@[defaultCategory]mutableCopy];
        [categoriesWithDefaultOption addObjectsFromArray:[MTLJSONAdapter modelsOfClass:[ProductCategory class] fromJSONArray:JsonCategories error:nil]];

        return categoriesWithDefaultOption;
    }]deliverOnMainThread];

    
}

@end
