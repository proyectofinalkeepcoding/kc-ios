//
//  CategoryService.h
//  FrikiPop
//
//  Created by Yago de la Fuente on 18/1/16.
//  Copyright © 2016 Saiyens. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface CategoryService : NSObject

-(RACSignal *)fetchCategories;

@end
