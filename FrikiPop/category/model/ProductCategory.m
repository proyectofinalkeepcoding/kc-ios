//
//  ProductCategory.m
//  FrikiPop
//
//  Created by Yago de la Fuente on 18/1/16.
//  Copyright © 2016 Saiyens. All rights reserved.
//

#import "ProductCategory.h"

@implementation ProductCategory

+(NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"name" : @"name",
             @"idCategory" : @"id"};
}

@end
