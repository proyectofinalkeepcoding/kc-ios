//
//  ProductCategory.h
//  FrikiPop
//
//  Created by Yago de la Fuente on 18/1/16.
//  Copyright © 2016 Saiyens. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>

@interface ProductCategory :  MTLModel<MTLJSONSerializing>

@property (strong, nonatomic) NSString *name;
@property int idCategory;

@end
