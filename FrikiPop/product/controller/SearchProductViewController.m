//
//  SearchProductViewController.m
//  FrikiPop
//
//  Created by Yago de la Fuente on 16/1/16.
//  Copyright © 2016 Saiyens. All rights reserved.
//

#import "SearchProductViewController.h"
#import "ProductsCollectionViewCell.h"
#import "ProductsCollectionViewDataSource.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "ProductStore.h"
#import "FilterParams.h"
#import "SuggestionsTableViewController.h"
#import "ProductService.h"
#import "ProductDetail.h"
#import "DetailProductViewController.h"
#import "NewProductViewController.h"
#import "SessionManager.h"
#import "LoginViewController.h"

@interface SearchProductViewController ()

@property (strong, nonatomic) ProductsCollectionViewDataSource *customDataSource;
@property (strong, nonatomic) FilterParams *filterParams;
@property (nonatomic, strong) UISearchController *searchController;
@property BOOL searchEnabled;
@property (strong,nonatomic) ProductService *productService;
@property (strong, nonatomic) UIRefreshControl *refreshControl;

@end

@implementation SearchProductViewController

static NSString * const cellReuseIdentifier = @"ProductCell";

-(id)initWithFilterParams: (FilterParams *) filterParams {
    
    if (self = [super initWithCollectionViewLayout:[UICollectionViewFlowLayout new]]) {
        _filterParams = filterParams;
        _searchEnabled = NO;
        self.title = self.filterParams.keywords;
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"filterIcon.png"]
                                                                                 style:UIBarButtonItemStylePlain
                                                                                target:self
                                                                                action:@selector(presentFilterView)];
    }
    return self;
}

-(id)initWithCollectionViewLayout:(UICollectionViewLayout *)layout {
    
    if (self = [super initWithCollectionViewLayout:[UICollectionViewFlowLayout new]]) {
        _filterParams = [FilterParams new];
        _searchEnabled = YES;
        self.title = @"Búsqueda";
      
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"filterIcon.png"]
                                                                                 style:UIBarButtonItemStylePlain
                                                                                target:self
                                                                                action:@selector(presentFilterView)];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                                                             target:self
                                                                                             action:@selector(addProduct)];
    }
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self configureView];
    [self configureSearchViewController];
    [self fetchProducts];
    // Login si el usuario no tiene un token guardado
    [self presentLoginController];
    
}

#pragma mark - UICollectionViewDataSource

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return [self.customDataSource numberOfSectionsInCollectionView:collectionView];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.customDataSource collectionView:collectionView
                          numberOfItemsInSection:section];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    return [self.customDataSource collectionView:collectionView
                          cellForItemAtIndexPath:indexPath];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(150, 200);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0.0, 18.0, 0.0, 18.0);
}

#pragma mark - UICollectionViewDelegate

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    Product *product = [self.customDataSource productForIndexPath:indexPath];
    
    RACSignal *detailSignal = [self.productService fecthProductDetail:product.idProduct];
    
    [detailSignal subscribeNext:^(ProductDetail *productDetail) {
        
        DetailProductViewController *vc = [[DetailProductViewController alloc]initWithProductDetail:productDetail];
        
        [self.navigationController pushViewController:vc
                                             animated:YES];
        
    }];
    
    [detailSignal subscribeError:^(NSError *error) {
        NSLog(@"Error al recuperar el detalle del producto %@", error);
    }];
}

#pragma mark FilterProductsDelegate

-(void) didEndFiltering:(int)idCategory radius:(float)radius keywords:(NSString *)keywords {
    [self dismissViewControllerAnimated:YES
                             completion:nil];
    
    self.filterParams.idCategory = idCategory;
    self.filterParams.radius = radius;
    self.filterParams.keywords = keywords;
    [self fetchProducts];
    [self cleanParams];

}

#pragma mark - UISearchBarDelegate

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self pushSuggestionControllerWithSuggestion:searchBar.text];
}

#pragma mark - OnSelectedSuggestionDelegate

-(void)didSelectSuggestion:(NSString *)suggestion {
    
    self.searchController.searchBar.text = @"";
    [self pushSuggestionControllerWithSuggestion:suggestion];
    
}


#pragma mark Private methods

-(void)configureView {
    
    self.collectionView.delegate = self;
    self.customDataSource = [ProductsCollectionViewDataSource new];
    
    self.collectionView.dataSource = self;
    UINib *cellNib = [UINib nibWithNibName:@"ProductsCollectionViewCell" bundle:nil];

    [self.collectionView registerNib:cellNib
          forCellWithReuseIdentifier:cellReuseIdentifier];
    
    self.collectionView.backgroundColor = [UIColor whiteColor];
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(fetchProducts)
             forControlEvents:UIControlEventValueChanged];
    [self.collectionView addSubview:self.refreshControl];
    
    _productService = [ProductService new];

}

-(void)addProduct {
    
    NewProductViewController *vc = [[NewProductViewController alloc]initWithNibName:nil
                                                                             bundle:nil];
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:vc];
    [self presentViewController:nav
                       animated:YES
                     completion:^{
                         [self fetchProducts];
                     }];
    
}

-(void)configureSearchViewController {
    
    if (self.searchEnabled) {
       
        SuggestionsTableViewController *vc = [[SuggestionsTableViewController alloc]initWithFilterParams:self.filterParams];
        vc.delegate = self;
        
        self.searchController = [[UISearchController alloc]initWithSearchResultsController:vc];
        self.searchController.searchResultsUpdater = vc;
        self.searchController.searchBar.placeholder = @"Búsqueda producto";
        self.searchController.searchBar.delegate = self;
        self.searchController.hidesNavigationBarDuringPresentation = NO;
        self.searchController.dimsBackgroundDuringPresentation = YES;
        self.navigationItem.titleView = self.searchController.searchBar;
        [self.searchController.searchBar sizeToFit];
        self.definesPresentationContext = YES;

    }
}

-(void)cleanParams {
    self.filterParams = [FilterParams new];
}

-(void)fetchProducts {
    
           [[self.customDataSource fetchProductsWithFilterParams:self.filterParams]subscribeNext:^(id x) {
            [self.collectionView reloadData];
               [self.refreshControl endRefreshing];
        }];
}

-(void)presentLoginController {
    
    
    NSString *token = [SessionManager sharedInstance].token;
    
    if (!token.length > 0) {
        
        LoginViewController *loginVC = [[LoginViewController alloc]initWithNibName:nil
                                                                            bundle:nil];
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:loginVC];
        [self presentViewController:nav
                           animated:YES
                         completion:nil];
        
    }
}

-(void)presentFilterView {
    
    FilterProductsViewController *vc = [[FilterProductsViewController alloc]init];
    vc.delegate = self;
    
    [self presentViewController:vc
                       animated:YES
                     completion:nil];
}

-(void)pushSuggestionControllerWithSuggestion: (NSString *)suggestion {
   
    self.filterParams.keywords = suggestion;
    SearchProductViewController *vc = [[SearchProductViewController alloc]initWithFilterParams:self.filterParams];
    [self.navigationController pushViewController:vc
                                         animated:YES];
}
@end
