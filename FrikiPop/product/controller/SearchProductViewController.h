//
//  SearchProductViewController.h
//  FrikiPop
//
//  Created by Yago de la Fuente on 16/1/16.
//  Copyright © 2016 Saiyens. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilterProductsViewController.h"
#import "SuggestionsTableViewController.h"
@class FilterParams;

@interface SearchProductViewController : UICollectionViewController <FilterProductsDelegate, OnSelectedSuggestionDelegate, UISearchBarDelegate>

-(id)initWithFilterParams: (FilterParams *) filterParams;

@end
