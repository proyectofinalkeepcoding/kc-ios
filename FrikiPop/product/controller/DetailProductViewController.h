//
//  DetailProductViewController.h
//  FrikiPop
//
//  Created by Yago de la Fuente on 23/1/16.
//  Copyright © 2016 Saiyens. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ProductDetail;

@interface DetailProductViewController : UIViewController <UIPageViewControllerDataSource, UIPageViewControllerDelegate>

-(id)initWithProductDetail: (ProductDetail *) detail;


@end
