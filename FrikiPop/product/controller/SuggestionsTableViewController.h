//
//  SuggestionsTableViewController.h
//  FrikiPop
//
//  Created by Yago de la Fuente on 19/1/16.
//  Copyright © 2016 Saiyens. All rights reserved.
//

#import <UIKit/UIKit.h>
@class FilterParams;

@protocol OnSelectedSuggestionDelegate

-(void)didSelectSuggestion: (NSString *)suggestion;

@end

@interface SuggestionsTableViewController : UITableViewController <UISearchResultsUpdating>

@property (weak, nonatomic) id<OnSelectedSuggestionDelegate> delegate;

-(id)initWithFilterParams: (FilterParams *) filterParams;

@end
