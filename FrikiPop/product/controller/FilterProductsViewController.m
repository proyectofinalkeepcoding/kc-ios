//
//  FilterProductsViewController.m
//  FrikiPop
//
//  Created by Yago de la Fuente on 18/1/16.
//  Copyright © 2016 Saiyens. All rights reserved.
//

#import "FilterProductsViewController.h"
#import "ProductCategory.h"
#import "CategoryService.h"
#import "UserService.h"
#import "User.h"
#import "UserProfile.h"
@import CoreLocation;

@interface FilterProductsViewController ()

@property (weak, nonatomic) IBOutlet UIPickerView *categoriesPicker;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (strong,nonatomic) NSArray *categories;
@property (strong, nonatomic) User *user;
@property (weak, nonatomic) IBOutlet UISlider *radiusSlider;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) MKCircle *circle;

@end

@implementation FilterProductsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.categoriesPicker.delegate = self;
    
    self.categoriesPicker.dataSource = self;
    [[[CategoryService new]fetchCategories] subscribeNext:^(NSArray *categories) {
        self.categories = categories;
        [self.categoriesPicker reloadAllComponents];
    }];
    
    [[[UserService new]fetchCurrentUser] subscribeNext:^(User *user) {
        self.user = user;
        [self updateMapView];
    }];
    
    self.mapView.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIPickerViewDataSource methods

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.categories.count;
}

#pragma mark - UIPickerViewDelegate methods

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    ProductCategory *category = self.categories[row];
    return category.name;
}

#pragma mark - MKMapViewDelegate

-(MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay {
    
    if ([overlay isKindOfClass:[MKCircle class]]) {
        MKCircleRenderer *circle = [[MKCircleRenderer alloc]initWithCircle:overlay];
        circle.strokeColor = [UIColor redColor];
        circle.fillColor = [UIColor colorWithRed:1
                                           green:0
                                            blue:0
                                           alpha:0.1];
        circle.lineWidth = 1;

        return circle;
    }
    return nil;
}

#pragma mark - Actions

-(void) addRadiusCircleToMap:(float)radius {
    

    [self.mapView removeOverlay:self.circle];
    CLLocationCoordinate2D location = CLLocationCoordinate2DMake(self.user.userProfile.latitude, self.user.userProfile.longitude);
    self.circle = [MKCircle circleWithCenterCoordinate:location
                                                     radius:radius*1000];
    [self.mapView addOverlay:self.circle];
    self.mapView.visibleMapRect = [self.mapView mapRectThatFits:self.circle.boundingMapRect edgePadding:UIEdgeInsetsMake(8, 8, 8, 8)];
    
}

- (IBAction)accept:(id)sender {
    NSUInteger rowSelected = [self.categoriesPicker selectedRowInComponent:0];
    ProductCategory *category = self.categories[rowSelected];
    [self.delegate didEndFiltering:category.idCategory
                            radius:self.radiusSlider.value
                          keywords:self.searchBar.text];
}

- (IBAction)radiusChanged:(id)sender {
    
    [self addRadiusCircleToMap:self.radiusSlider.value];
    
}

#pragma mark - Private methods

-(void)updateMapView {
    MKCoordinateRegion region;
    region.center.latitude = self.user.userProfile.latitude;
    region.center.longitude = self.user.userProfile.longitude;
    region.span.latitudeDelta = 0.001;
    region.span.longitudeDelta = 0.001;
    MKPointAnnotation *pin = [[MKPointAnnotation alloc] init];
    pin.coordinate = CLLocationCoordinate2DMake(self.user.userProfile.latitude, self.user.userProfile.longitude);
    pin.title = @"Usuario";
    [self.mapView addAnnotation:pin];
    [self.mapView setRegion:region
                   animated:true];
    
    [self addRadiusCircleToMap:self.radiusSlider.value];

}

@end
