//
//  SuggestionsTableViewController.m
//  FrikiPop
//
//  Created by Yago de la Fuente on 19/1/16.
//  Copyright © 2016 Saiyens. All rights reserved.
//

#import "SuggestionsTableViewController.h"
#import "FilterParams.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "ProductService.h"
#import "SearchProductViewController.h"

@interface SuggestionsTableViewController ()
@property (strong, nonatomic) FilterParams *filterParams;
@property (strong, nonatomic) NSArray *suggestions;
@property (strong, nonatomic) ProductService *service;
@end

static NSString *const reuseIdentifier = @"SuggestionCell";

@implementation SuggestionsTableViewController

-(id)initWithFilterParams: (FilterParams *) filterParams {
    
    if (self = [super initWithStyle:UITableViewStylePlain]) {
        _filterParams = filterParams;
        _service = [ProductService new];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
   
    
    [self.tableView registerClass:UITableViewCell.class
           forCellReuseIdentifier:reuseIdentifier];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return self.suggestions.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier forIndexPath:indexPath];
    cell.textLabel.text = self.suggestions[indexPath.row];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
   
    [self.delegate didSelectSuggestion:self.suggestions[indexPath.row]];
     self.suggestions = @[];
}

#pragma mark - UISearchResultsUpdating

-(void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    
    if (searchController.searchBar.text.length > 2) {
        self.filterParams.keywords = searchController.searchBar.text;
        [[self.service fetchProductSuggestions:self.filterParams]subscribeNext:^(NSArray *suggestions) {
            self.suggestions = suggestions;
            [self.tableView reloadData];
        }];
    }
}

@end
