//
//  NewProductViewController.h
//  FrikiPop
//
//  Created by Yago de la Fuente on 24/1/16.
//  Copyright © 2016 Saiyens. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewProductViewController : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate,UIPickerViewDelegate, UIPickerViewDataSource>

@end
