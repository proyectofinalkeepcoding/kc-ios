//
//  FilterProductsViewController.h
//  FrikiPop
//
//  Created by Yago de la Fuente on 18/1/16.
//  Copyright © 2016 Saiyens. All rights reserved.
//

#import <UIKit/UIKit.h>
@import MapKit;

@protocol FilterProductsDelegate

-(void) didEndFiltering:(int)idCategory radius:(float)radius keywords:(NSString *)keywords;

@end

@interface FilterProductsViewController : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource, MKMapViewDelegate>

@property (weak, nonatomic) id<FilterProductsDelegate> delegate;

@end
