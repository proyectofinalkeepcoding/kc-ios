//
//  DetailProductViewController.m
//  FrikiPop
//
//  Created by Yago de la Fuente on 23/1/16.
//  Copyright © 2016 Saiyens. All rights reserved.
//

#import "DetailProductViewController.h"
@import MapKit;
@import CoreLocation;
#import "ProductDetail.h"
#import "UserProfile.h"
#import "Image.h"
#import <UIImageView+AFNetworking.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface DetailProductViewController ()
@property (weak, nonatomic) IBOutlet UIView *containerPageView;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImage;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *productDate;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) ProductDetail *detail;
@property (strong, nonatomic) UIPageViewController *pageView;
@property (strong, nonatomic) NSMutableArray *images;
@property int indexViewPage;
        

@end

@implementation DetailProductViewController

-(id)initWithProductDetail: (ProductDetail *) detail {
    
    if (self = [super initWithNibName:nil
                               bundle:nil]) {
        _detail = detail;
        _images = [@[]mutableCopy];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self updateMapView];
    [self configureView];
    [self configurePageViewController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIPageViewControllerDataSource

-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    
    if (self.indexViewPage == self.detail.images.count - 1) {
        return nil;
    }
    self.indexViewPage++;
    return self.images[self.indexViewPage];

}

-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    
    if (self.indexViewPage == 0) {
        return nil;
    }
    self.indexViewPage--;
    return self.images[self.indexViewPage];
    
}

#pragma mark - UIPageViewControllerDelegate

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    // The number of items reflected in the page indicator.
    if (self.detail.images.count == 0) {
        return 1;
    }
    return self.detail.images.count;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    // The selected item reflected in the page indicator.
    return self.indexViewPage;
}


#pragma mark - Private methods

-(void)updateMapView {
    MKCoordinateRegion region;
    region.center.latitude = self.detail.seller.userProfile.latitude;
    region.center.longitude = self.detail.seller.userProfile.longitude;
    region.span.latitudeDelta = 0.001;
    region.span.longitudeDelta = 0.001;
    MKPointAnnotation *pin = [[MKPointAnnotation alloc] init];
    pin.coordinate = CLLocationCoordinate2DMake(self.detail.seller.userProfile.latitude, self.detail.seller.userProfile.longitude);
    pin.title = @"Usuario";
    [self.mapView addAnnotation:pin];
    [self.mapView setRegion:region
                   animated:true];
    
}

-(void)configureView {
    
    self.title = self.detail.name;
    self.avatarImage.layer.cornerRadius = self.avatarImage.frame.size.height /2;
    self.avatarImage.layer.masksToBounds = YES;
    self.avatarImage.layer.borderWidth = 0;
    [self.avatarImage setImageWithURL:[NSURL URLWithString:self.detail.seller.userProfile.avatarURL]];
    
    self.usernameLabel.text = self.detail.seller.username;
    self.priceLabel.text = [NSString stringWithFormat:@"%.02f€", self.detail.price];
    self.productDate.text = self.detail.publishDate;
    self.descriptionLabel.text = self.detail.productDescription;
//    self.tabBarController.tabBar.hidden = YES;
}

-(void)configurePageViewController {
    
    self.pageView = [[UIPageViewController alloc]initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll
                                   navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal
                                                 options:nil];
    
    UIPageControl *pageControl = [UIPageControl appearance];
    pageControl.pageIndicatorTintColor = [UIColor blackColor];
    pageControl.currentPageIndicatorTintColor = [UIColor blueColor];
    [self addChildViewController:self.pageView];
    self.pageView.view.frame = self.containerPageView.bounds;
    [self.containerPageView addSubview:self.pageView.view];
    self.pageView.dataSource = self;
    self.pageView.delegate = self;
    [self configureImagesForPageController];
    [self.pageView didMoveToParentViewController:self];

}

-(void)configureImagesForPageController {
    
    if (self.detail.images.count == 0) {
        UIViewController *vc = [[UIViewController alloc]initWithNibName:nil
                                                                 bundle:nil];
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:self.containerPageView.bounds];
 
        imageView.image = [UIImage imageNamed:@"detail_image_placeholder.png"];
        imageView.contentMode = UIViewContentModeCenter;
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        vc.view = imageView;
        [self.images addObject:vc];
    }
    
    for (Image *image in self.detail.images) {
        UIViewController *vc = [[UIViewController alloc]initWithNibName:nil
                                                                 bundle:nil];
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:self.containerPageView.bounds];

        [imageView setImageWithURL:[NSURL URLWithString:image.urlImage]
                  placeholderImage:[UIImage imageNamed:@"detail_image_placeholder.png"]];
        imageView.contentMode = UIViewContentModeCenter;
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        vc.view = imageView;
        [self.images addObject:vc];

    }
    @weakify(self);
       [self.pageView setViewControllers:[NSArray arrayWithObject:[self.images firstObject]]
                            direction:UIPageViewControllerNavigationDirectionForward
                             animated:YES
                           completion:^(BOOL finished) {
                               @strongify(self);
                               [self.pageView reloadInputViews];
                           }];

    
}

@end
