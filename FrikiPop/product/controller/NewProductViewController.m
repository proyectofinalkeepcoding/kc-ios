//
//  NewProductViewController.m
//  FrikiPop
//
//  Created by Yago de la Fuente on 24/1/16.
//  Copyright © 2016 Saiyens. All rights reserved.
//

#import "NewProductViewController.h"
#import "CategoryService.h"
#import "ProductCategory.h"
#import "NewProductDetail.h"
#import "ProductService.h"
#import "Azure.h"
#import "ProductDetail.h"

@interface NewProductViewController ()
@property (weak, nonatomic) IBOutlet UITextField *nameLabel;
@property (weak, nonatomic) IBOutlet UITextView *descriptionLabel;
@property (weak, nonatomic) IBOutlet UITextField *priceLabel;
@property (weak, nonatomic) IBOutlet UIImageView *firstImageView;
@property (weak, nonatomic) IBOutlet UIImageView *secondImageView;
@property (weak, nonatomic) IBOutlet UIImageView *thirdImageView;
@property (weak, nonatomic) IBOutlet UIImageView *fourthImageView;
@property BOOL firstImage;
@property BOOL secondImage;
@property BOOL thirdImage;
@property (weak, nonatomic) IBOutlet UIPickerView *categoryPicker;
@property (strong, nonatomic) NSArray *categories;
@property (strong, nonatomic) Azure *azure;

@end

@implementation NewProductViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _azure = [Azure sharedInstance];
    
    self.categoryPicker.delegate = self;
    
    // Carga categorias
    self.categoryPicker.dataSource = self;
    [[[CategoryService new]fetchCategories] subscribeNext:^(NSArray *categories) {
        self.categories = categories;
        [self.categoryPicker reloadAllComponents];
    }];
    
    [self configureTaps];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                                                         target:self
                                                                                         action:@selector(cancelNewProduct)];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemSave
                                                                                          target:self
                                                                                          action:@selector(saveProduct)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.fd
}


-(void)cancelNewProduct {
    
    [self.presentingViewController dismissViewControllerAnimated:YES
                                                      completion:nil];
    
}

-(void)saveProduct {
    
    NewProductDetail *detail = [NewProductDetail new];
    
    detail.name = self.nameLabel.text;
    detail.productDescription = self.descriptionLabel.text;
    detail.idCategory = (int)[self.categoryPicker selectedRowInComponent:0];
    detail.price = [self.priceLabel.text floatValue];
    RACSignal *uploadProductSignal = [[ProductService new] uploadProduct:detail];
    [uploadProductSignal subscribeNext:^(NSString *idProduct) {
        
        if (self.firstImageView.image) {
            NSData *data = UIImageJPEGRepresentation(self.firstImageView.image
                                                     , 0.1);
            NSString *uuid = [[NSUUID UUID] UUIDString];
            [self.azure saveBlobWithName:[NSString stringWithFormat:@"%@_%@", detail.name, uuid] data:data completion:^(NSURL *url) {
               
                if (url) {
                     detail.idProduct = [idProduct intValue];
                    [[[ProductService new] updateProduct:detail
                                         urlDefaultImage:url.absoluteString]subscribeError:^(NSError *error) {
                        NSLog(@"Error al actualizar producto : %@", error);
                    }];
                }
            }];
        }
        
    }];
    
    [uploadProductSignal subscribeError:^(NSError *error) {
        NSLog(@"Error al subir el producto %@", error);
    }];
    
    [[self presentingViewController] dismissViewControllerAnimated:YES
                                                        completion:nil];
    
    
}

#pragma mark - UIImagePickerControllerDelegate

-(void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    UIImage *img = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    if (!self.firstImage) {
        self.firstImageView.image = img;
        self.firstImage = YES;
    } else if (!self.secondImage) {
        self.secondImageView.image = img;
        self.secondImage = YES;
    } else if (!self.thirdImage) {
        self.thirdImageView.image = img;
        self.thirdImage = YES;
    } else {
        self.fourthImageView.image = img;
    }
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

-(void) showPhotoView{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    
    picker.delegate = self;
    picker.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Foto"
                                                                       message:@"Seleccione el origen"
                                                                preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction *camara = [UIAlertAction actionWithTitle:@"Cámara" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:picker animated:YES completion:nil];
        }];
        
        UIAlertAction *galery = [UIAlertAction actionWithTitle:@"Galería" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:picker animated:YES completion:nil];
        }];
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancelar" style:UIAlertActionStyleCancel handler:nil];
        
        [alert addAction:camara];
        [alert addAction:galery];
        [alert addAction:cancel];
        
        [alert setModalPresentationStyle:UIModalPresentationPopover];
        [self presentViewController:alert animated:YES completion:nil];
        
    }else{
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:nil];
    }
}

#pragma mark - UIPickerViewDataSource methods

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.categories.count;
}

#pragma mark - UIPickerViewDelegate methods

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    ProductCategory *category = self.categories[row];
    return category.name;
}

#pragma mark Private methods

-(void)configureTaps {
    
    // Do any additional setup after loading the view from its nib.
    self.firstImageView.userInteractionEnabled = YES;
    self.secondImageView.userInteractionEnabled = YES;
    self.thirdImageView.userInteractionEnabled = YES;
    self.fourthImageView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self
                                                                         action:@selector(showPhotoView)];
    [self.firstImageView addGestureRecognizer:tap];
    tap = [[UITapGestureRecognizer alloc]initWithTarget:self
                                                 action:@selector(showPhotoView)];
    [self.secondImageView addGestureRecognizer:tap];
    tap = [[UITapGestureRecognizer alloc]initWithTarget:self
                                                 action:@selector(showPhotoView)];
    [self.thirdImageView addGestureRecognizer:tap];
    tap = [[UITapGestureRecognizer alloc]initWithTarget:self
                                                 action:@selector(showPhotoView)];
    [self.fourthImageView addGestureRecognizer:tap];
    
    
}

@end
