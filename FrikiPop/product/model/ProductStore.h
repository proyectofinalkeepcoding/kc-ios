//
//  ProductStore.h
//  FrikiPop
//
//  Created by Yago de la Fuente on 14/1/16.
//  Copyright © 2016 Saiyens. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "Product.h"
#import "FilterParams.h"

@interface ProductStore : NSObject

-(NSArray *)allItems;

-(NSUInteger)numberOfItems;

-(RACSignal *)fetchUserProducts;

-(Product *) productForIndexPath:(NSIndexPath *)indexPath;

-(RACSignal *)fetchProductsWithFilterParams:(FilterParams *)filterParams;


@end
