//
//  Product.m
//  FrikiPop
//
//  Created by Yago de la Fuente on 14/1/16.
//  Copyright © 2016 Saiyens. All rights reserved.
//

#import "Product.h"

@implementation Product

+(NSDictionary *)JSONKeyPathsByPropertyKey {
    
    return @{@"idProduct" : @"id",
             @"name" : @"name",
             @"price" : @"price",
             @"defaultImageURL" : @"default_image_url"};
}

@end
