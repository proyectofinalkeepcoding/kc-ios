//
//  Product.h
//  FrikiPop
//
//  Created by Yago de la Fuente on 14/1/16.
//  Copyright © 2016 Saiyens. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>

@interface Product : MTLModel<MTLJSONSerializing>

@property int idProduct;
@property (copy, nonatomic) NSString *defaultImageURL;
@property (copy, nonatomic) NSString *name;
@property float price;

@end
