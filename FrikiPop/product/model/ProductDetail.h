//
//  ProductDetail.h
//  FrikiPop
//
//  Created by Yago de la Fuente on 23/1/16.
//  Copyright © 2016 Saiyens. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>
#import "User.h"

@interface ProductDetail : MTLModel<MTLJSONSerializing>

@property int idProduct;
@property (strong, nonatomic) User *seller;
@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *productDescription;
@property (copy, nonatomic) NSString *state;
@property (copy, nonatomic) NSString *publishDate;
@property float price;
@property int idCategory;
@property (strong, nonatomic) NSArray *images;

@end
