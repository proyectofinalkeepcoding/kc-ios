//
//  ProductDetail.m
//  FrikiPop
//
//  Created by Yago de la Fuente on 23/1/16.
//  Copyright © 2016 Saiyens. All rights reserved.
//

#import "ProductDetail.h"

@implementation ProductDetail

+(NSDictionary *)JSONKeyPathsByPropertyKey {
    
    return @{@"idProduct" : @"id",
             @"seller" : @"seller",
             @"name" : @"name",
             @"productDescription" : @"description",
             @"state" : @"state",
             @"publishDate" : @"publish_date",
             @"price" : @"price",
             @"idCategory" : @"category"};
    
}

@end
