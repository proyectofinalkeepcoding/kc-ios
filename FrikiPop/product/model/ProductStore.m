//
//  ProductStore.m
//  FrikiPop
//
//  Created by Yago de la Fuente on 14/1/16.
//  Copyright © 2016 Saiyens. All rights reserved.
//

#import "ProductStore.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "ProductService.h"

@interface ProductStore ()

@property (strong, nonatomic) NSArray *items;

@end

@implementation ProductStore

#pragma mark Warehouse methods

-(NSArray *)allItems {
    return self.items;
}

-(NSUInteger)numberOfItems {
    return self.items.count;
}

-(Product *)productForIndexPath:(NSIndexPath *)indexPath {
    return [self.items objectAtIndex:indexPath.row];
}

#pragma mark Fetching methods

-(RACSignal *)fetchUserProducts {
    
    ProductService *service = [ProductService new];
    RACSignal *productsSignal = [service fetchUserProducts];

    return [productsSignal map:^id(NSArray *products) {
        return self.items = products;
    }];

}

-(RACSignal *)fetchProductsWithFilterParams:(FilterParams *)filterParams {
    
    ProductService *service = [ProductService new];
    RACSignal *productsSignal = [service fetchProductsWithFilter:filterParams];
    
    return [productsSignal map:^id(NSArray *products) {
        return self.items = products;
    }];
    
}
@end
