//
//  FilterParams.h
//  FrikiPop
//
//  Created by Yago de la Fuente on 19/1/16.
//  Copyright © 2016 Saiyens. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FilterParams : NSObject

@property (copy, nonatomic) NSString *keywords;
@property int idCategory;
@property float radius;

@end
