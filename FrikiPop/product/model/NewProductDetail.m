//
//  NewProductDetail.m
//  FrikiPop
//
//  Created by Yago de la Fuente on 24/1/16.
//  Copyright © 2016 Saiyens. All rights reserved.
//

#import "NewProductDetail.h"

@implementation NewProductDetail

+(NSDictionary *)JSONKeyPathsByPropertyKey {
    
    return @{
             @"name" : @"name",
             @"productDescription" : @"description",
             @"price" : @"price",
             @"idCategory" : @"category"};
    
}

@end
