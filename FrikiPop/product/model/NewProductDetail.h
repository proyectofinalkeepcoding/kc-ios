//
//  NewProductDetail.h
//  FrikiPop
//
//  Created by Yago de la Fuente on 24/1/16.
//  Copyright © 2016 Saiyens. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>

@interface NewProductDetail : MTLModel<MTLJSONSerializing>

@property int idProduct;
@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *productDescription;
@property float price;
@property int idCategory;
@end
