//
//  ProductsCollectionDelegate.m
//  FrikiPop
//
//  Created by Yago de la Fuente on 14/1/16.
//  Copyright © 2016 Saiyens. All rights reserved.
//

#import "ProductsCollectionViewDataSource.h"
#import "ProductStore.h"
#import "ProductsCollectionViewCell.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "Product.h"

@interface ProductsCollectionViewDataSource ()

@property (strong, nonatomic) ProductStore *store;

@end

@implementation ProductsCollectionViewDataSource

-(id)init {
    
    if (self = [super init]) {
        _store = [ProductStore new];
    }
    return self;
}

#pragma mark DataSource

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ProductsCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ProductCell"
                                                                                 forIndexPath:indexPath];
    Product *product = [self.store productForIndexPath:indexPath];
    
    cell.layer.cornerRadius = 5;
    cell.layer.masksToBounds = YES;
    
    [cell configureCell:product];
    
    return cell;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.store.numberOfItems;
}

#pragma mark Fetching methods

-(RACSignal *)fetchUserProducts {
    return [[self.store fetchUserProducts]deliverOnMainThread];
}

-(RACSignal *)fetchProductsWithFilterParams:(FilterParams *)filter {
    return [[self.store fetchProductsWithFilterParams:filter]deliverOnMainThread];
}

-(Product *)productForIndexPath: (NSIndexPath *) indexPath {
    return [self.store productForIndexPath:indexPath];
}



@end
