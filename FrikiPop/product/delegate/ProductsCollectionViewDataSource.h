//
//  ProductsCollectionDelegate.h
//  FrikiPop
//
//  Created by Yago de la Fuente on 14/1/16.
//  Copyright © 2016 Saiyens. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "FilterParams.h"
@class Product;
@import UIKit;

@interface ProductsCollectionViewDataSource : NSObject <UICollectionViewDataSource>

-(RACSignal *)fetchUserProducts;

-(RACSignal *)fetchProductsWithFilterParams:(FilterParams *)filter;

-(Product *)productForIndexPath: (NSIndexPath *) indexPath;


@end
