//
//  ProductsCollectionViewCell.m
//  FrikiPop
//
//  Created by Yago de la Fuente on 14/1/16.
//  Copyright © 2016 Saiyens. All rights reserved.
//

#import "ProductsCollectionViewCell.h"
#import <UIImageView+AFNetworking.h>

static NSString* DEFAULT_IMAGE = @"default_image.png";

@implementation ProductsCollectionViewCell

-(void)configureCell:(Product *)product {
    
    
    self.descriptionLabel.text = product.name;
    self.prizeLabel.text = [NSString stringWithFormat:@"%.02f€", product.price];
    [self.photoView setImageWithURL:[NSURL URLWithString:product.defaultImageURL]
                   placeholderImage:[UIImage imageNamed:@"no_product_image"]];
    
}

- (void)awakeFromNib {
    // Initialization code
}

-(void)prepareForReuse {
    //self.photoView.image = nil;
    self.descriptionLabel.text = nil;
    self.prizeLabel.text = nil;
}

@end
