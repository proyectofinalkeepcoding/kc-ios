//
//  ProductService.h
//  FrikiPop
//
//  Created by Yago de la Fuente on 15/1/16.
//  Copyright © 2016 Saiyens. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "FilterParams.h"
@class ProductDetail;
@class NewProductDetail;

@interface ProductService : NSObject

-(RACSignal *)fetchUserProducts;

-(RACSignal *)fetchProductsWithFilter:(FilterParams *)filter;

-(RACSignal *)fetchProductSuggestions:(FilterParams *)filter;

-(RACSignal *)fecthProductDetail: (int)idProduct;

-(RACSignal *)uploadProduct: (NewProductDetail *)detail;

-(RACSignal *)updateProduct: (NewProductDetail *)detail urlDefaultImage: (NSString *)url;

@end
