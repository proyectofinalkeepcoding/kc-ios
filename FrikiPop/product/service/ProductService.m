//
//  ProductService.m
//  FrikiPop
//
//  Created by Yago de la Fuente on 15/1/16.
//  Copyright © 2016 Saiyens. All rights reserved.
//

#import "ProductService.h"
#import "ApiClient.h"
#import <Mantle/Mantle.h>
#import "Product.h"
#import "ServiceUtils.h"
#import "SessionManager.h"
#import "ProductDetail.h"
#import "Image.h"
#import "NewProductDetail.h"

static NSString *PRODUCTS_PATH = @"api/1.0/products/";
static NSString *IMAGES_PATH = @"api/1.0/images/";

@interface ProductService ()

@property (strong, nonatomic) ApiClient *apiClient;

@end

@implementation ProductService

-(id)init {
    
    if (self = [super init]) {
        _apiClient = [ApiClient sharedInstance];
    }
    return self;
}

-(RACSignal *)fetchUserProducts {
    
    NSString *username = [SessionManager sharedInstance].username;
    RACSignal *userProductsSignal = [self.apiClient GET:PRODUCTS_PATH
                                             withParams:@{@"seller" : username}
                                     withAuthentication:YES];
    
    return [userProductsSignal map:^id(NSArray *productJson) {
        
        return [MTLJSONAdapter modelsOfClass:Product.class
                               fromJSONArray:productJson
                                       error:nil];
        
    }];
    
}

-(RACSignal *)fetchProductsWithFilter:(FilterParams *)filter {
    
    NSDictionary *params = [self dictionaryFromFilterParams:filter];
    
    RACSignal *productsSignal = [self.apiClient GET:PRODUCTS_PATH
                                         withParams:params
                                 withAuthentication:YES];
    
    return [productsSignal map:^id(NSArray *productJson) {
        
        return [MTLJSONAdapter modelsOfClass:Product.class
                               fromJSONArray:productJson
                                       error:nil];
    }];
}

-(RACSignal *)fetchProductSuggestions:(FilterParams *)filter {
    
    RACSignal *productsSignal = [self fetchProductsWithFilter:filter];
   
    NSMutableArray *suggestions = [@[]mutableCopy];
    
    return [[productsSignal map:^id(NSArray *products) {
    
        for (Product *product in products) {
            [suggestions addObject:product.name];
        }
        
        return suggestions;
    }]deliverOnMainThread];
    
}

-(RACSignal *)fecthProductDetail: (int)idProduct {
    
    
    NSString *productPath = [ServiceUtils addParamToPath:PRODUCTS_PATH
                                               parameter:[NSString stringWithFormat:@"%d", idProduct]];
    
    RACSignal *productsSignal = [self.apiClient GET:productPath
                                         withParams:nil
                                 withAuthentication:YES];
    
    return [[productsSignal flattenMap:^RACStream *(NSDictionary *product) {
        
        ProductDetail *detail = [MTLJSONAdapter modelOfClass:[ProductDetail class]
                                          fromJSONDictionary:product
                                                       error:nil];
        
        return [[self.apiClient GET:IMAGES_PATH
                         withParams:@{@"product" : [NSString stringWithFormat:@"%d", detail.idProduct]}
                 withAuthentication:YES] map:^id(id value) {
            if ([value isKindOfClass:[NSDictionary class]]) {
                Image *image = [MTLJSONAdapter modelOfClass:[Image class]
                                         fromJSONDictionary:value
                                                      error:nil];
                detail.images = [NSArray arrayWithObject:image];
            } else {
                detail.images = [MTLJSONAdapter modelsOfClass:[Image class]
                                                fromJSONArray:value
                                                        error:nil];
            }
            
            return detail;
        }];
        
    }]deliverOnMainThread];
    
}

-(RACSignal *)uploadProduct: (NewProductDetail *)detail {
    
    NSDictionary *params = [MTLJSONAdapter JSONDictionaryFromModel:detail
                                                             error:nil];

    return [[self.apiClient POST:PRODUCTS_PATH
              withParams:params
      withAuthentication:YES] map:^id(NSDictionary *productDetail) {
        return [NSString stringWithFormat:@"%@", productDetail[@"id"]];
    }];
}

-(void) saveImage: (int) idProduct urlImage: (NSString *)urlImage {
    
    [self.apiClient POST:IMAGES_PATH
              withParams:@{@"url" : urlImage,
                           @"product" : [NSString stringWithFormat:@"%d", idProduct]}
      withAuthentication:YES];
    
}

-(RACSignal *)updateProduct: (NewProductDetail *)detail urlDefaultImage: (NSString *)url {
    
    NSDictionary *params = @{@"name" : detail.name,
                             @"price" : [NSString stringWithFormat:@"%f", detail.price],
                             @"category" : [NSString stringWithFormat:@"%d", detail.idCategory],
                             @"default_image_url" : url
                             };
   
    return [self.apiClient PUT:[ServiceUtils addParamToPath:PRODUCTS_PATH
                                                  parameter:[NSString stringWithFormat:@"%d", detail.idProduct]]
              withParams:params
             withAuthentication:YES];
    
}


#pragma mark - Private methods

-(NSDictionary *)dictionaryFromFilterParams: (FilterParams *)filter {
    NSMutableDictionary *dict = [@{}mutableCopy];
    
    if (filter) {
        if (filter.keywords) {
            [dict addEntriesFromDictionary:@{@"keywords" : filter.keywords}];
        }
        if (filter.idCategory != 0) {
            [dict addEntriesFromDictionary:@{@"category" : [NSString stringWithFormat:@"%d", filter.idCategory]}];
        }
    }
    return dict;
}

@end
