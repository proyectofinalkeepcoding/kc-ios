//
//  AppDelegate.h
//  FrikiPop
//
//  Created by Pawel Walicki on 12/11/15.
//  Copyright © 2015 Saiyens. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

