//
//  ServiceUtils.h
//  FrikiPop
//
//  Created by Yago de la Fuente on 16/1/16.
//  Copyright © 2016 Saiyens. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ServiceUtils : NSObject

+(NSString *) addParamToPath: (NSString *)path parameter: (NSString *) parameter;

@end
