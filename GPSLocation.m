//
//  GPSLocation.m
//  FrikiPop
//
//  Created by Yago de la Fuente on 4/2/16.
//  Copyright © 2016 Saiyens. All rights reserved.
//

#import "GPSLocation.h"

@interface GPSLocation ()
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (nonatomic, copy) void (^completionBlock)(CLLocation *currentLocation);
@end

@implementation GPSLocation

-(id)init {
    
    if (self = [super init]) {
        _locationManager = [[CLLocationManager alloc]init];
        self.locationManager.delegate = self;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        
    }
    return self;
}

-(void)currentLocationWithBlock:(void(^)(CLLocation *currentLocation))completion{
    
    
    self.completionBlock = completion;
    [self.locationManager requestWhenInUseAuthorization];
    [self.locationManager startUpdatingLocation];
    
    
    
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    
    
    CLLocation *currentLocation = [locations lastObject];
    
    if (currentLocation) {
        self.completionBlock(currentLocation);
    }
    
    [self.locationManager stopUpdatingLocation];
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    
    NSLog(@"Error al localizar al usuario %@", error);
    [self.locationManager stopUpdatingLocation];
}

@end
